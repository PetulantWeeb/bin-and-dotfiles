# bin and dotfiles

## Symlink Working Directories

`ln -s $PWD/bin ~/.local/bin`

`ln -s $PWD/bashrc.d ~/.bashrc.d`

## Fix Keychron Fn Keys

1. `sudo cp ./systemd/keychron.service /etc/systemd/system/`
2. `systemctl enable keychron`
3. `systemctl start keychron`

Read the manual fool:

1. fn + X + L = Switch between function and multimedia keys (F1-F12).
2. fn + K + C = F5 & F6 key switch (When in Mac/iOS Mode).
3. fn + S + O = Disable/Enable Auto Sleep Mode.
4. fn + Light Bulb Key = Turn off the Backlight.

