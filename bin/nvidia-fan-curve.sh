#!/bin/bash

# For this to work:
# 1. Edit '/etc/X11/xorg.conf'
# 2. Find 'Section "Device"'
# 3. Add to the bottom of the section:
# '  Option  "Coolbits" "4"'
# OR
# run 'sudo nvidia-xconfig --cool-bits=4'

targetSpeed=0

while true
do
    currentTemp=($(nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader,nounits))
	currentSpeed=($(nvidia-smi --query-gpu=fan.speed --format=csv,noheader,nounits))
    
    # Idle
    if [ $currentTemp -lt 50 ]
    then
        targetSpeed=30
	# Doing Something
    elif [ $currentTemp -lt 60 ]
    then
    	targetSpeed=50
	# A bit toasty...
    elif [ $currentTemp -lt 70 ]
    then
    	targetSpeed=65
	# Target Temperature approaching (81 C)
    elif [ $currentTemp -lt 80 ]
    then
        targetSpeed=80
	# Max/Slowdown/Shutdown Temps reached (89 C, 91 C, 94 C)
    else
        targetSpeed=100
    fi
    
    if [ $currentSpeed -ne $targetSpeed ]
    then
        nvidia-settings -a [gpu:0]/GPUFanControlState=1 -a [fan:0]/GPUTargetFanSpeed=$targetSpeed
        nvidia-settings -a [gpu:0]/GPUFanControlState=1 -a [fan:1]/GPUTargetFanSpeed=$targetSpeed
        currentSpeed=$targetSpeed
    fi
    
    sleep 30

done
