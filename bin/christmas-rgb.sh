#!/bin/bash

echo "Script running infinitly. Press [Ctrl + C] to stop."

while true
do
	openrgb --profile ChristmasLights1 >/dev/null 2>&1
	sleep 1
	openrgb --profile ChristmasLights2 >/dev/null 2>&1
	sleep 1
	openrgb --profile ChristmasLights3 >/dev/null 2>&1
	sleep 1
	openrgb --profile ChristmasLights4 >/dev/null 2>&1
	sleep 1
done
