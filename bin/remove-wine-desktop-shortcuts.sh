#!/bin/bash

echo "Removing ~/.local/share/applications/wine directory"
rm -rf ~/.local/share/applications/wine

echo "Cleaning up ~/.local/share/applications/"
rm -f ~/.local/share/applications/wine-*
rm -f ~/.local/share/applications/mimeinfo.cache

echo "Cleaning up /.config/menus/applications-merged/"
rm -f ~/.config/menus/applications-merged/wine*

echo "DONE"
