#!/bin/bash

for f in *.flac; do
	echo "Cleaning up $f";
	for t in "$f"; do
	metaflac \
	--show-tag title \
	--show-tag artist \
	--show-tag albumartist \
	--show-tag album \
	--show-tag discnumber \
	--show-tag date \
	--show-tag tracknumber \
	--show-tag tracktotal \
	--show-tag genre \
	--show-tag description \
	--show-tag composer \
	--show-tag performer \
	"$t" | metaflac --preserve-modtime --remove-all-tags --import-tags-from=- "$t"
	done
done

echo
echo "Show flac files comments"
metaflac *.flac --list | grep "comments"

